  //Pin connected to OUTPUT_ENABLE of 5841BN
int latchPin = 11;
//Pin connected to CLOCK of 5841BN
int clockPin = 9;
////Pin connected to SERIAL_DATA_IN of 5841BN
int dataPin = 10;

void setup() {
  //set pins to output because they are addressed in the main loop
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
    
    Serial.begin(9600);
  
  
   /* byte state_rele=3;
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, state_rele);  
    digitalWrite(latchPin, HIGH);
    delay(2000);
    byte mask=1;
    int pin=4;
    mask =mask<<pin;
    state_rele=state_rele | mask;
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, LSBFIRST, state_rele);  
    digitalWrite(latchPin, HIGH);
    Serial.println(state_rele);*/
    
  
}

void loop() {
  //count up routine
  for (int j = 0; j < 256; j++) {
    //ground latchPin and hold low for as long as you are transmitting
    digitalWrite(latchPin, LOW);
    shiftOut(dataPin, clockPin, MSBFIRST, j);  
    //return the latch pin high to signal chip that it
    //no longer needs to listen for information
    digitalWrite(latchPin, HIGH);
    delay(1000);
  }
  delay(1000);
} 
