// Simple demo of three threads
// LED blink thread, print thread, and main thread
//#include <ChibiOS_AVR.h>
#include <Keypad.h>
#include "RTClib.h"
/*-----( Import needed libraries )-----*/
#include <Wire.h>  // Comes with Arduino IDE
// Get the LCD I2C Library here: 
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move any other LCD libraries to another folder or delete them
// See Library "Docs" folder for possible commands etc.
// connection http://arduino-info.wikispaces.com/LCD-Blue-I2C
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>

#define DEBUG_ON

#define HORA_I 0
#define MIN_I 1
#define HORA_F 2
#define MIN_F 3
#define LIMIT 4
#define RELE 5
#define SENSOR 6
#define ENABLE 7
#define DELAY_V 8

#define ESPERA_VENTANA 10000



const uint8_t LED_PIN = 13;

volatile uint32_t count = 0;
int count_key=0;
boolean new_key=false;
boolean new_screen = false;

boolean delay_s =true;

//-----------------------Inicializacion keypad-------------------------------


const byte ROWS = 3; //four rows
const byte COLS = 5; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'7','8','9','0','A'},
  {'4','5','6','B','C'},
  {'1','2','3','D','E'},
};
byte rowPins[ROWS] = {26, 24, 22}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {36,34, 32, 30, 28}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

char customKey;

uint8_t r_hour=0,r_min=0, num=0;

//-------------------------Inicializacion LCD---------------------------------

/*-----( Declare objects )-----*/
// set the LCD address to 0x20 for a 20 chars 4 line display
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

int id_screen = 0;
int id_subscreen = 0, id_unidad=0;
int addr_EEPROM = 0;

//------------------------Inicializacion 5841BN (shift register)---------------
//Pin connected to OUT_ENALBE of 5841BN
const int OutEnablePin = 12;
//Pin connected to STROBE of 5841BN
const int latchPin = 11;
//Pin connected to CLOCK of 5841BN
const int clockPin = 9;
////Pin connected to SERIAL_DATA_IN of 5841BN
const int dataPin = 10;
//--------------------------RTC-------------------
RTC_DS1307 rtc;
DateTime now; // Obtiene la fecha y hora del RTC
//--------------------------ADC------------
volatile int ADC_values[8]={};
//---------------------


char data_screen_1[7];
// Hora-min- 
char index_data_1=0;
boolean refresh_screen=true;

//byte state_rele_1 = false, state_rele_2 = false;
byte state_rele = false;
//byte state_rele[8] ={false,false,false,false,false,false,false,false};



unsigned long time_1=0;
unsigned long time_2=0;
unsigned long time_3=0;
unsigned long time_4=0;
unsigned long time_5=0;
unsigned long temp=0;

//------------------------- Funcion control rele ------------------
void control_rele(byte & out_state, byte num_rele, boolean activate){
  byte mask=1; 
  
  if (activate){
    mask = mask << num_rele;
    out_state=out_state | mask;
  }
  
  else{
    mask = mask << num_rele;
    mask = ~mask;
    out_state=out_state & mask;
    }
  //Update state rele
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, out_state);  
  digitalWrite(latchPin, HIGH);
}

//----------------------------------------
boolean time_compare(byte hora_ini, byte min_ini, byte hora_fin, byte min_fin ){
  // es true si la hora actual esta dentro del intervalo.
  boolean res=false;
  boolean cambiar=false;
  
  // si la hora final es menor que la hora inicial (caso 21:00 -> 01:00) se intercambia la hora inicial por la final y 
  // se niega el intervalo en el que se busca que este la hora
  if (hora_ini>hora_fin){
    int hora_temp;
    int min_temp;
    
    hora_temp=hora_fin;
    hora_fin=hora_ini;
    hora_ini=hora_temp;
    
    min_temp=min_fin;
    min_fin=min_ini;
    min_ini=min_temp;
    
    cambiar=true;
  }
   
     
  if( hora_ini<=now.hour() && hora_fin>=now.hour()){
    if( hora_ini<now.hour() && hora_fin>now.hour()){
        res=true;
      }
    else if ( hora_ini==now.hour() && min_ini<=now.minute() ){
          res=true;
      }
     else if(hora_fin==now.hour() &&  min_fin>now.minute()){
          res=true;
      }
  }
  
  if (cambiar) res=not(res);
  
  
  return res;
}


//----------------------------------------


boolean getHour(char Key, int & id_unidad, char lcd_c, char lcd_f){
boolean state= false;
switch(id_unidad){
      
      case 0:
        lcd.setCursor(lcd_c,lcd_f+0); 
        lcd.print(Key);
        data_screen_1[index_data_1]=(Key-0x30)*10;
        id_unidad=1;
        break;
      case 1:
        lcd.setCursor(lcd_c+1,lcd_f+0); 
        lcd.print(Key);
        data_screen_1[index_data_1]=(Key-0x30)+data_screen_1[index_data_1];
        id_unidad=2;
        index_data_1++;
        break;
      case 2:
         lcd.setCursor(lcd_c+3,lcd_f+0); 
         lcd.print(Key);
         data_screen_1[index_data_1]=(Key-0x30)*10;
         id_unidad = 3;
         break;
      case 3:
         lcd.setCursor(lcd_c+4,lcd_f+0); 
         lcd.print(Key);
         data_screen_1[index_data_1]=(Key-0x30)+data_screen_1[index_data_1];
         id_unidad =0 ;
         index_data_1++;
         
         Serial.println(data_screen_1[index_data_1]);
         Serial.println(data_screen_1[index_data_1-1]);
         state=true;
         
         break;
      }
return state;
}

//------------------------------------------------------------------------------
boolean getNumber(char Key, int & id_unidad, char lcd_c, char lcd_f){
boolean state= false;
switch(id_unidad){
      
      case 0:
        lcd.setCursor(lcd_c,lcd_f+0); 
        lcd.print(Key);
        data_screen_1[index_data_1]=(Key-0x30)*10;
        id_unidad=1;
        break;
      case 1:
        lcd.setCursor(lcd_c+1,lcd_f+0); 
        lcd.print(Key);
        data_screen_1[index_data_1]=(Key-0x30)+data_screen_1[index_data_1];
        id_unidad=0;
        index_data_1++;
         
        Serial.println(data_screen_1[index_data_1]);
        state=true;
         
        break;
      }
return state;
}

//----------------------------------------------------------------------------
/*Rutina de normalizacion para el sensor anemometro
*/
int CalibracionAnenometro(int16_t value){
  value = ((value-160)*100)/800;
  if (value < 0) value = 0;
return value;
}

int CalibracionVeleta(int16_t value){
  
  value = (4.8*value -637.7 )/10;
  if (value < 0) value = 0;

return value;
}

//-----------------------------------------------------------------------------
/*Rutina para controlar la apertura de las ventanas. El control de las ventanas esta supeditado a dos condiciones: 
la velocidad del viento (anemometro) y a la hora de funcionamiento*/

void ControlVentana(uint8_t EEPROM_base, byte & out_state){
  boolean comp_hour, comp_value;
  uint16_t valSensor;
  
  //lectura analogica entrada y normalizacion del valor del sensor. Se suma 7 para coincidir con la numeracion de los ADCs del micro
  valSensor = CalibracionAnenometro(analogRead(EEPROM.read(EEPROM_base + SENSOR) +7));
  //Comparacion del valor del sensor con la referencia
  (valSensor <= EEPROM.read(EEPROM_base + LIMIT)) ? comp_value = true : comp_value = false;
 
 //Comparacion de la hora actual con las referencias
 comp_hour = time_compare(EEPROM.read(EEPROM_base + HORA_I), EEPROM.read(EEPROM_base + MIN_I), EEPROM.read(EEPROM_base + HORA_F), EEPROM.read(EEPROM_base + MIN_F));
 
 //Actuacion sobre el rele. Se actua sobre dos reles consecutivos (Abrir y cerrar ventena)  
   
   
     control_rele(out_state, EEPROM.read(EEPROM_base + RELE) -1, comp_value & comp_hour & EEPROM.read(EEPROM_base + DELAY_V));
     control_rele(out_state, EEPROM.read(EEPROM_base + RELE)  , !(comp_value & comp_hour & EEPROM.read(EEPROM_base + DELAY_V)));
 Serial.println(valSensor);
/*Filtro para envitar que la ventana se habra y se cierre continuamente. Si la ventana se cierra 
se tiene que esperar el tiempo ESPERA_VENTANA hasta que se vuelva a abrir  */
   if (!comp_value){
     time_5=millis();
     EEPROM.write(EEPROM_base + DELAY_V,0);
   }
   else if (comp_value && (millis()-time_5>ESPERA_VENTANA)) {
     EEPROM.write(EEPROM_base + DELAY_V,1);
   }
   
  
   
   Serial.println(delay_s);
 
 #ifdef DEBUG_ON
 Serial.print("----------------------------- Ventana: ");
 Serial.println(EEPROM_base/10);
 Serial.print("Inter hora: ");
 Serial.println(comp_hour);
 Serial.print("Comp valor: ");
 Serial.println(comp_value);
 Serial.print("Entrada Sensor: ");
 Serial.println(EEPROM.read(EEPROM_base + SENSOR));
 Serial.print("ValSensor: ");
 Serial.println(valSensor);
 (comp_value & comp_hour) ? Serial.println("Activo"): Serial.println("Desactivado");
 #endif
 
}



//------------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
  lcd.begin(20,4); 
  rtc.begin();
  // initialize the lcd for 20 chars 4 lines and turn on backlight
  // wait for USB Serial
  while (!Serial) {}
  
  // read any input
  delay(200);
  while (Serial.read() >= 0) {}
  
  lcd.backlight(); // finish with backlight on  
  
  //set pins 5841BN
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  
  time_4=millis();

}





void loop(){
  char Key =0;
  boolean state_time;
  
  
  if (millis()>=temp){
    
    temp = millis();
    }
  else{
     time_1=0;
     time_2=0;
     time_3=0;
     time_4=0;
     temp=millis();
  }
  
  
  
  Key = customKeypad.getKey();
    if (Key)customKey=Key;
  
  if(customKeypad.keyStateChanged()){
            count_key++;  
            time_1=millis();
        }  
  
  if((millis()-time_1)>500 && count_key>3){
      count_key = 0;
      new_key=true;
      Serial.println(new_key);
      
      lcd.setBacklight(1);
      time_4=millis();
      
  }
  
  /*Proceso control pantalla*/
  if (new_key || ((millis()-time_2)>1000)){
      Screen();
      refresh_screen=true;
      new_key=false;
      time_2=millis();
      
  }
  
  /*Proceso actualizacion leyes de controls y salida reles*/
  if((millis()-time_3)>1000){
    
    for (int i=0;i<4;i++){
        if (EEPROM.read(i*10+7)) ControlVentana(i*10, state_rele) ;
        
        else {
          control_rele(state_rele, EEPROM.read(i*10 + 5) -1, false);
          control_rele(state_rele, EEPROM.read(i*10 + 5), false);
        }
    }
    
    time_3=millis();
  }
  
  /*Control iluminacion pantalla*/
  if((millis()-time_4)>30000){
    lcd.setBacklight(0);
  }
}


void Screen(){
  
 
  switch (id_screen){
    
    case 0:
         if(refresh_screen){
                    // Print count for previous second.
              lcd.setCursor(5,0);
              lcd.print("JIHERMA SL");
              
               //---show time
              
              now = rtc.now(); // Obtiene la fecha y hora del RTC
              
              lcd.setCursor(16,1);
              lcd.print(now.second()+100, DEC); // Segundos
              lcd.setCursor(16,1);
              lcd.print(':');
              lcd.setCursor(13,1);
              lcd.print(now.minute()+100, DEC); // Minutos
              lcd.setCursor(13,1);
              lcd.print(':');
              lcd.setCursor(10,1);
              lcd.print(now.hour()+100, DEC); // Horas
              lcd.setCursor(10,1);
              lcd.print(' ');
              
              lcd.setCursor(5,1);
              lcd.print(now.year(), DEC); // Año
              lcd.setCursor(6,1);
              lcd.print('/');
              lcd.setCursor(3,1);
              lcd.print(now.month()+100, DEC); // Mes
              lcd.setCursor(3,1);
              lcd.print('/');
              lcd.setCursor(0,1);
              lcd.print(now.day()+100, DEC); // Dia
              lcd.setCursor(0,1);
              lcd.print(' ');
              
              
              
              
              
              
              
              /*
              lcd.setCursor(0,1);
              lcd.print(now.day(), DEC); // Dia
              lcd.print('/');
              lcd.print(now.month(), DEC); // Mes
              lcd.print('/');
              lcd.print(now.year(), DEC); // Año
              lcd.print(' ');
              lcd.print(now.hour(), DEC); // Horas
              lcd.print(':');
              lcd.print(now.minute(), DEC); // Minutos
              lcd.print(':');
              lcd.print(now.second(), DEC); // Segundos
              */
              lcd.setCursor(0,3);
              lcd.print("Program:");
              lcd.setCursor(9,3);
              lcd.print(EEPROM.read(ENABLE) ? "V1 " :"" );
              lcd.print(EEPROM.read(10+ENABLE) ? "V2 " : "");
              lcd.print(EEPROM.read(20+ENABLE) ? "V3 " : "");
              lcd.print(EEPROM.read(30+ENABLE) ? "V4 " : "");
              
 
              refresh_screen=false;
              id_unidad = 0;
              id_subscreen = 0;
              index_data_1=0;
         }
          
            
            //Programas 1-4 para el control de las ventanas
            if (customKey >= '1' && customKey <='4'){
                id_screen = 5; 
                addr_EEPROM = ((customKey -0x30)-1)*10; 
                
                refresh_screen=false;
                new_screen = true;
                lcd.clear();
            
            }
            

          
                
          // Configuracion de hora y fecha
          else if(new_key && customKey =='C') {
                  id_screen = 3; 
                  refresh_screen=false;
                  new_screen = true;
                  lcd.clear();
          }
          
          else if(new_key && customKey == 'B'){
            lcd.clear();
            id_screen = 7;
            new_screen = true; 
            
            } 
            

      break;
      //Pantalla para introducir la hora y el limite de velocidad del viento
    case 1:
      
      if (new_screen){              
              lcd.setCursor(0,0);  
              lcd.print("Hora inicio: --:--");
              lcd.setCursor(0,1); 
              lcd.print("Hora fin   : --:--");
              lcd.setCursor(0,2); 
              lcd.print("Lim vel: --%");
              lcd.setCursor(6,3); 
              lcd.print("Siguiente(ENT)");
              
              new_screen = false;
              id_unidad = 0;
              id_subscreen = 0;
              index_data_1=0;
             
      }
      
      
      if(new_key && customKey>=0x30 && customKey<=0x39){
        Serial.println("pantalla 1 - conf");
            
          switch(id_subscreen){
              case 0:
                if( getHour(customKey, id_unidad, 13 , 0))  {
                  id_subscreen++;
                  }
                break;
              case 1: 
                if( getHour(customKey, id_unidad, 13 , 1)){
                  id_subscreen++;
                  }
                break;
              case 2: 
                if(getNumber(customKey, id_unidad, 9, 2)){ 
                  id_subscreen++;
                 
                  }
                break;
          }
      }
      if(new_key && customKey == 'D' && id_subscreen == 3){
            id_screen = 2;
            new_screen = true;
            lcd.clear();
      }
  
      else if(new_key && customKey =='A'){
            id_screen = 5;  
            new_screen = true;
            lcd.clear();
      }
      
      break;
  
  case 2: 

      if (new_screen){
              lcd.setCursor(0,0);  
              lcd.print("N. salida rele: --");
              lcd.setCursor(0,1); 
              lcd.print("N. entr analog: --");
              lcd.setCursor(8,3); 
              lcd.print("Guardar(ENT)");
              
              new_screen = false;
              id_unidad = 0;
              id_subscreen = 0;
              
      }  
      
      if(new_key && customKey>=0x30 && customKey<=0x39){
        Serial.println("pantalla 2 - conf");
            
          switch(id_subscreen){
              case 0:
                if( getNumber(customKey, id_unidad, 16 , 0))  {
                  id_subscreen=1;
                  }
                break;
              
              case 1: 
                if( getNumber(customKey, id_unidad, 16 , 1)){
                  id_subscreen=2;
                  }
                break;
  
          }
      }
      
      if(new_key && customKey=='D' && id_subscreen==2){
            id_screen = 0;
            new_screen = true;
            lcd.clear();
                  
                  for(int i=0;i<=6;i++){
                    // Hora_i, Min_i, Hora_f, Min_f, lim_vel, salida rele, entrada lectura analog
                     EEPROM.write(addr_EEPROM+i, data_screen_1[i]);
                     Serial.println(i);
                  }
                  
                  for(int i=0;i<=6;i++){
                     Serial.println(EEPROM.read(addr_EEPROM + i));
                  }
                  
                  Serial.println("Datos Guardados");
                  
                 
      }
      
      if(new_key && customKey=='A'){
            id_screen = 5;  
            new_screen = true;
            lcd.clear();
        
      }
        
      
      break;
      
   case 3: //Mostrar opciones de configuracion
   
     if (new_screen){
              lcd.setCursor(0,0);  
              lcd.print("1. Config. Reloj");
              
              new_screen = false;
              id_unidad = 0;
              id_subscreen = 0;
              index_data_1=0;
              
      } 
     
     if(new_key && customKey == '1'){
            id_screen = 4;
            new_screen = true;
            lcd.clear();
      }
      
     else if(new_key && customKey == 'A'){
        id_screen = 0;
        new_screen = true;
        lcd.clear();
      
      }
   
   
     break;
   
   case 4:
   
     if (new_screen){
              lcd.setCursor(0,0);  
              lcd.print("Hora: --:--");
              lcd.setCursor(0,1); 
              lcd.print("Fecha: --/--/--");
              lcd.setCursor(0,3); 
              lcd.print("Guardar (Entrar)");
              
              new_screen = false;
              id_unidad = 0;
              id_subscreen = 0;
              index_data_1=0;
      }
      
      if(new_key && customKey>=0x30 && customKey<=0x39){
        Serial.println("Conf fecha");
            
          switch(id_subscreen){
              case 0:
                if( getHour(customKey, id_unidad, 6 , 0))  {
                  id_subscreen++;
                  }
                break;
              case 1: 
                if( getNumber(customKey, id_unidad, 7 , 1)){
                  id_subscreen++;
                  }
                break;
              case 2: 
                if(getNumber(customKey, id_unidad, 10, 1)){ 
                  id_subscreen++;
                  }
                break;
              case 3: 
                if(getNumber(customKey, id_unidad, 13, 1)){ 
                  id_subscreen++;
                  }
                break;
                
          }
      }
      
      else if(new_key && customKey == 'A'){
        id_screen = 3;
        new_screen = true;
        lcd.clear();
      
      }
      
      else if(new_key && customKey == 'D' && id_subscreen == 4){
            
           int year_rtc = 2000 + data_screen_1[4];
                     // January 21, 2014 at 3am you would call:
                     // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
            #ifdef DEBUG_ON
              Serial.print("Anno: ");
              Serial.println(year_rtc);
              Serial.print("mes: ");
              Serial.println(data_screen_1[3],DEC);
              Serial.print("dia: ");
              Serial.println(data_screen_1[2],DEC);
              Serial.print("hora: ");
              Serial.println(data_screen_1[0],DEC);
              Serial.print("min: ");
              Serial.println(data_screen_1[1],DEC);
            #endif
            
            rtc.adjust(DateTime(year_rtc, data_screen_1[3], data_screen_1[2], data_screen_1[0], data_screen_1[1] , 0));
            Serial.println("Fecha guardada");
            
            lcd.clear();      
            id_screen = 3;
            new_screen = true;

        
      }
     break;

     case 5:
      
      if (new_screen){
              byte mask= 0b00000011;
              byte res;
              res = (state_rele >> addr_EEPROM*2/10) & mask ;
              lcd.setCursor(0,0);
            lcd.print("Vent ");
            lcd.print(addr_EEPROM/10 +1,DEC);
            lcd.print(": ");
            if (res ==0x00 ) lcd.print("desactivada ");
            else if (res == 0x01) lcd.print("abierta ");
            else if (res ==0x02 ) lcd.print("cerrada ");
            
              lcd.setCursor(0,1); 
              lcd.print("Sensor: ");
              lcd.print(EEPROM.read(addr_EEPROM + SENSOR),DEC); 
              
              lcd.setCursor(0,2);
              lcd.print("Rele: ");
              lcd.print(EEPROM.read(addr_EEPROM + RELE),DEC);
                
              lcd.setCursor(6,3); 
              lcd.print("Siguiente(ENT)");
              lcd.setCursor(14,3); 
              lcd.print("");
              
              new_screen = false;
              id_unidad = 0;
              id_subscreen = 0;
              index_data_1=0;
      }

      if(new_key && customKey == 'D'){
            lcd.clear();
            id_screen = 6;
            new_screen = true;
            
      }
  
      
      else if(new_key && customKey == 'A'){
            lcd.clear();
            id_screen = 0;
            new_screen = true;          
      }    
      break;
      
      case 6:
      
      if (new_screen){
              
              lcd.print("Hora inicio: ");
              lcd.setCursor(15,0); 
              lcd.print(EEPROM.read(addr_EEPROM + MIN_I)+100,DEC);
              lcd.setCursor(15,0); 
              lcd.print(":");
              lcd.setCursor(12,0);   
              lcd.print(EEPROM.read(addr_EEPROM + HORA_I)+100,DEC);
              lcd.setCursor(12,0); 
              lcd.print(" ");
             
              lcd.setCursor(0,1);
              lcd.print("Hora fin: ");
              lcd.setCursor(12,1); 
              lcd.print(EEPROM.read(addr_EEPROM + MIN_F)+100,DEC);
              lcd.setCursor(12,1); 
              lcd.print(":");
              lcd.setCursor(9,1);   
              lcd.print(EEPROM.read(addr_EEPROM + HORA_F)+100,DEC);
              lcd.setCursor(9,1); 
              lcd.print(" ");
              
              lcd.setCursor(0,2);
              lcd.print("Lim: ");
              lcd.print(EEPROM.read(addr_EEPROM + LIMIT),DEC);

              lcd.setCursor(0,3); 
              lcd.print("On(9) Off(0) Ed(FUN)");
              lcd.setCursor(14,3); 
              lcd.print("");
              
              new_screen = false;
              id_unidad = 0;
              id_subscreen = 0;
              index_data_1=0;
      }

      if(new_key && customKey == 'C'){
            lcd.clear();
            id_screen = 1;
            new_screen = true;
            
      }
  
      else if(new_key && (customKey==0x30 || customKey==0x39)){
            
            if (customKey =='9') EEPROM.write(addr_EEPROM + ENABLE, 1);
            else if (customKey =='0') EEPROM.write(addr_EEPROM + ENABLE, 0);
            
            lcd.clear();
            id_screen = 0;  
            new_screen = true;      
      }
      else if(new_key && customKey == 'A'){
            lcd.clear();
            id_screen = 0;
            new_screen = true;          
      }    
      break;
      
      case 7:
         if(refresh_screen){
                    // Print count for previous second.
              lcd.setCursor(0,0);
              lcd.print("Entrada sensores");
              
              
              lcd.setCursor(1,1);
              lcd.print(analogRead(8),DEC);
              lcd.print(' ');
              lcd.print(analogRead(9),DEC);
              lcd.print(' ');
              lcd.print(analogRead(10),DEC);
              lcd.print(' ');
              lcd.setCursor(1,2);
              lcd.print(analogRead(11),DEC);
              lcd.print(' ');
               lcd.print(analogRead(12),DEC);
              lcd.print(' ');
              lcd.print(analogRead(13),DEC);
              lcd.print(' ');
              lcd.setCursor(1,3);
              lcd.print(analogRead(14),DEC);
              lcd.print(' ');
               lcd.print(analogRead(15),DEC);
              lcd.print(' ');
              
              refresh_screen=false;
              id_unidad = 0;
              id_subscreen = 0;
              index_data_1=0;
         }
          
            
            if(new_key && customKey == 'A'){
            lcd.clear();
            id_screen = 0;
            new_screen = true;          
            } 
            

      break;
  }
}


