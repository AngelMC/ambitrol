// Simple demo of three threads
// LED blink thread, print thread, and main thread
#include <ChibiOS_AVR.h>
#include <Keypad.h>
#include "RTClib.h"
/*-----( Import needed libraries )-----*/
#include <Wire.h>  // Comes with Arduino IDE
// Get the LCD I2C Library here: 
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move any other LCD libraries to another folder or delete them
// See Library "Docs" folder for possible commands etc.
// connection http://arduino-info.wikispaces.com/LCD-Blue-I2C
#include <LiquidCrystal_I2C.h>




const uint8_t LED_PIN = 13;

volatile uint32_t count = 0;

//-----------------------Inicializacion keypad-------------------------------


const byte ROWS = 3; //four rows
const byte COLS = 5; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'7','8','9','0','A'},
  {'4','5','6','B','C'},
  {'1','2','3','D','E'},
};
byte rowPins[ROWS] = {26, 24, 22}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {36,34, 32, 30, 28}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

char customKey;

//-------------------------Inicializacion LCD---------------------------------

/*-----( Declare objects )-----*/
// set the LCD address to 0x20 for a 20 chars 4 line display
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

int id_screen = 0;
int count_key = 0;

//------------------------Inicializacion 5841BN (shift register)---------------
//Pin connected to OUTPUT_ENABLE of 5841BN
const int latchPin = 11;
//Pin connected to CLOCK of 5841BN
const int clockPin = 9;
////Pin connected to SERIAL_DATA_IN of 5841BN
const int dataPin = 10;
//--------------------------RTC-------------------
RTC_DS1307 rtc;
DateTime now; // Obtiene la fecha y hora del RTC
//--------------------------ADC------------
volatile int ADC_values[8]={};
//---------------------
void control_rele(byte & out_state, byte num_rele, boolean activate){
  byte mask=1; 
  
  if (activate){
    mask = mask << num_rele;
    out_state=out_state | mask;
  }
  
  else{
    mask = mask << num_rele;
    mask = ~mask;
    out_state=out_state & mask;
  }
  //Update state rele
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, LSBFIRST, out_state);  
  digitalWrite(latchPin, HIGH);
}

boolean time_compare(byte hora_ini, byte min_ini, byte hora_fin, byte min_fin ){

  boolean res=false;
  boolean cambiar=false;
  
  // si la hora final es menor que la hora inicial (caso 21:00 -> 01:00) se intercambia la hora inicial por la final y 
  // se niega el intervalo en el que se busca que este la hora
  if (hora_ini>hora_fin){
    int hora_temp;
    int min_temp;
    
    hora_temp=hora_fin;
    hora_fin=hora_ini;
    hora_ini=hora_temp;
    
    min_temp=min_fin;
    min_fin=min_ini;
    min_ini=min_temp;
    
    cambiar=true;
  }
   
     
  if( hora_ini<=now.hour() && hora_fin>=now.hour()){
    if( hora_ini<now.hour() && hora_fin>now.hour()){
        res=true;
      }
    else if ( hora_ini==now.hour() && min_ini<=now.minute() && min_fin>now.minute()){
          res=true;
      }
     else if(hora_fin==now.hour() && min_ini<=now.minute() && min_fin>now.minute()){
          res=true;
      }
  }
  
  if (cambiar) res=not(res);
  
  
  return res;
}

//------------------------------------------------------------------------------
// thread 1 - high priority for blinking LED
// 64 byte stack beyond task switch and interrupt needs
static WORKING_AREA(waThread1, 64);

static msg_t Thread1(void *arg) {
  pinMode(LED_PIN, OUTPUT);
  
  // Flash led every 200 ms.
  while (1) {
    // Turn LED on.
    digitalWrite(LED_PIN, HIGH);
    
    // Sleep for 50 milliseconds.
    chThdSleepMilliseconds(50);
    
    // Turn LED off.
    digitalWrite(LED_PIN, LOW);
    
    // Sleep for 150 milliseconds.
    chThdSleepMilliseconds(150);
  }
  return 0;
}



//------------------------------------------------------------------------------
// thread 2 - print main thread count every second
// 100 byte stack beyond task switch and interrupt needs
static WORKING_AREA(waThread2, 120);

static msg_t Thread2(void *arg) {

  // print count every second
  
  
  
  while (1) {
    // Sleep for one second.
    chThdSleepMilliseconds(250);
    char Key=0;
     Key = customKeypad.getKey();
    if (Key)customKey=Key;
    
    switch (id_screen){
    
    case 0:
              // Print count for previous second.
        lcd.setCursor(3,0); //Start at character 4 on line 0
        lcd.print(F("Count: "));
        lcd.print(count);
        lcd.setCursor(3,1);
        
          lcd.print(customKey);
          
          if (customKey =='A'){
            lcd.clear();
            id_screen = 1; 
            
            
          }
        
        //---show time
        
        now = rtc.now(); // Obtiene la fecha y hora del RTC
        lcd.setCursor(1,2);
        lcd.print(now.year(), DEC); // Año
        lcd.print('/');
        lcd.print(now.month(), DEC); // Mes
        lcd.print('/');
        lcd.print(now.day(), DEC); // Dia
        lcd.print(' ');
        lcd.print(now.hour(), DEC); // Horas
        lcd.print(':');
        lcd.print(now.minute(), DEC); // Minutos
        lcd.print(':');
        lcd.print(now.second(), DEC); // Segundos
        
        lcd.setCursor(1,4);
        lcd.print(ADC_values[0],DEC);
        lcd.print(' ');
        lcd.print(ADC_values[1],DEC);
        lcd.print(' ');
        
        //time compare(hour_ini, min_ini, hour_fin, min_fin)-> true si la hora actual esta en el intervalo
        if (time_compare(0,1,0,3))
          lcd.print("true");
        else
          lcd.print("false");
          break;
    case 1:
    
    
      lcd.setCursor(1,0); 
      lcd.print(customKey);
      if(customKeypad.keyStateChanged()){
          count_key++;
      }
      if (count_key>=5){
      lcd.print(customKey);
      count_key = 0 ;
      }
      
      //lcd.print(customKeypad.getKey());
      
      if (customKey =='B'){
          
            id_screen = 0;  
            lcd.clear();
          }
      break;
    }

    
    
    /*
    // Print unused stack for threads.
    Serial.print(F(", Unused Stack: "));
    Serial.print(chUnusedStack(waThread1, sizeof(waThread1)));
    Serial.print(' ');
    Serial.print(chUnusedStack(waThread2, sizeof(waThread2))); 
    Serial.print(' ');
    Serial.println(chUnusedHeapMain());
*/
    // Zero count.
    count = 0;
  }
}

//-----------------------------------------------------------------------------
// thread 3 - read keypad values 
static WORKING_AREA(waThread3, 64);
  
static msg_t Thread3(void *arg) {
  byte state_out=0;
  char Key=0;
  while (1) {
     //Key = customKeypad.getKey();
    //if (Key)customKey=Key;
    
    control_rele(state_out,(customKey-48),true);
    // Sleep for 150 milliseconds.
    chThdSleepMilliseconds(250);
  }
  return 0;
}

//-----------------------------------------------------------------------------
// thread 4 - read ADC
static WORKING_AREA(waThread4, 64);

static msg_t Thread4(void *arg) {
  
  byte i=0;
  
  
  while (1) {
     for (i=0;i<8;i++){
      ADC_values[i]=analogRead(i);
  }
    // Sleep for 150 milliseconds.
    chThdSleepMilliseconds(200);
  }
  return 0;
}

//------------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
  lcd.begin(20,4); 
  rtc.begin();
  // initialize the lcd for 20 chars 4 lines and turn on backlight
  // wait for USB Serial
  while (!Serial) {}
  
  // read any input
  delay(200);
  while (Serial.read() >= 0) {}
  
  lcd.backlight(); // finish with backlight on  
  
  //set pins 5841BN
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  
  chBegin(mainThread);
  // chBegin never returns, main thread continues with mainThread()
  while(1) {}
}
//------------------------------------------------------------------------------
// main thread runs at NORMALPRIO
void mainThread() {

  // start blink thread
  //chThdCreateStatic(waThread1, sizeof(waThread1),
  //                        NORMALPRIO + 2, Thread1, NULL);

  // start print thread
  chThdCreateStatic(waThread2, sizeof(waThread2),
                          NORMALPRIO + 1, Thread2, NULL);
                       
  // start keypad thread
  chThdCreateStatic(waThread3, sizeof(waThread3),
                          NORMALPRIO + 2, Thread3, NULL);
                      
  // start ADC
  chThdCreateStatic(waThread4, sizeof(waThread4),
                          NORMALPRIO + 1, Thread4, NULL);

  // increment counter
  while (1) {
    // must insure increment is atomic in case of context switch for print
    // should use mutex for longer critical sections
    noInterrupts();
    count++;
    interrupts();
  }
}
//------------------------------------------------------------------------------
void loop() {
 // not used
}
